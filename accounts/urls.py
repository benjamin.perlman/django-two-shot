from django import views
from django.urls import path

from django.contrib.auth import views as auth_views

from django.contrib.auth import authenticate, login

# from django.views import SignUpView
from accounts.views import signup


urlpatterns = [
    # path("", ReceiptListView.as_view(), name="home"),
    # path("admin/", admin.site.urls),
    #    path("login/", Login, name="login"),
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("signup/", signup, name="signup"),
]
