from django.urls import path

from receipts.views import (
    AccountCreateView,
    AccountListView,
    CategoryCreateView,
    CategoryListView,
    ReceiptListView,
    ReceiptCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_new"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path("categories/", CategoryListView.as_view(), name="category_list"),
    path("accounts/create/", AccountCreateView.as_view(), name="account_new"),
    path(
        "categories/create/", CategoryCreateView.as_view(), name="category_new"
    ),
]
