from django.db import IntegrityError
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods
from django.db.models import Sum, Count


# from recipes.forms import RecipeForm
from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):  # LoginRequiredMixin
    model = Receipt
    template_name = "receipts/newrec.html"
    fields = [
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
    ]

    success_url = reverse_lazy("home")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class CategoryCreateView(LoginRequiredMixin, CreateView):  # LoginRequiredMixin
    model = ExpenseCategory
    template_name = "receipts/newcat.html"
    fields = ["name"]
    success_url = reverse_lazy("home")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)
    def form_valid(self, form):
        item = form.save(commit=False)
        item.ownerr = self.request.user
        item.save()
        return redirect("category_list")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/newaccount.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("home")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

    def form_valid(self, form):
        item = form.save(commit=False)
        item.ownerr = self.request.user
        item.save()
        return redirect("account_list")


class CategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/listcat.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(ownerr=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["total"] = ExpenseCategory.objects.annotate(
            num=Count("receipts")
        )
        return context


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/listaccount.html"

    def get_queryset(self):
        return Account.objects.filter(ownerr=self.request.user)
