from django.db import models

# from django.db.models import Sum, Count

# Create your models here.
# from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateField()

    # description = models.TextField()
    category = models.ForeignKey(
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    purchaser = models.ForeignKey(
        USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.vendor)


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    ownerr = models.ForeignKey(
        USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )
    # sum = Receipt.objects.count()

    # print(sum)
    #    print(pubs)

    def __str__(self):
        return str(self.name)


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    # sum = Receipt.objects.count()
    ownerr = models.ForeignKey(
        USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )
    # print(sum)

    def __str__(self):
        return str(self.name)
