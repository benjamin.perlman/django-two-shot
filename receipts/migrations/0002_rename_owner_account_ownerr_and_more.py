# Generated by Django 4.0.3 on 2022-05-05 23:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('receipts', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='account',
            old_name='owner',
            new_name='ownerr',
        ),
        migrations.RenameField(
            model_name='expensecategory',
            old_name='owner',
            new_name='ownerr',
        ),
    ]
